@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO DE CLIENTE</div>
                <div class="col text-right">
                    <a href="{{route('list.cliente') }}" class="btn btn-sm btn-primary">Cancelar</a>
                </div>

                <div class="card-body">
                
                <form role="form" method ="post" action="{{ route('guardar.clientes')}}">
            
                      {{csrf_field() }}
                    {{method_field("post") }}

                    <div class="row">
                    <div class="col-lg-4">
                     <label class="form-control-label" for="nombre">Nombre del cliente </label>
                     <input type="text" class="from-control" name="nombre">
                    </div>

                  
                    <div class="col-lg-4">
                     <label class="form-control-label" for="apellidos">Apellidos del cliente </label>
                     <input type="text" class="from-control" name="apellidos">
                    </div>

                    <div class="col-lg-4">
                     <label class="form-control-label" for="cedula">cedula del cliente </label>
                     <input type="number" class="from-control" name="cedula">
                    </div>

                  
                    <div class="col-lg-4">
                      <label class="form-control-label" for="direccion">direccion del cliente </label>
                     <input type="text" class="from-control" name="direccion">
                    </div>

                  
                    <div class="col-lg-4">
                     <label class="form-control-label" for="telefono">telefono del cliente </label>
                     <input type="number" class="from-control" name="telefono">
                    </div>

                  
                    <div class="col-lg-4">
                     <label class="form-control-label" for="fecha_nacimiento">fecha nacimiento del cliente </label>
                     <input type="text" class="from-control" name="fecha_nacimiento">
                    </div>

                  
                    <div class="col-lg-4">
                     <label class="form-control-label" for="email">email del cliente </label>
                     <input type="text" class="from-control" name="email">
                    </div>

                  <button type="submit" class="btn btn-success pull-right"> Guardar </button>

                </form>
                
    


                </div>
            </div>
        </div>
    </div>
</div>
@endsection